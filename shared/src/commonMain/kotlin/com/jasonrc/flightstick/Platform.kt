package com.jasonrc.flightstick

interface Platform {
    val name: String
}

expect fun getPlatform(): Platform